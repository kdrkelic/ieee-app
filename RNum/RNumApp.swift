//
//  RNumApp.swift
//  RNum
//
//  Created by Katarina on 23/01/2024.
//

import SwiftUI
import Firebase 

@main
struct RNumApp: App {
    init(){
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
