//
//  UserData.swift
//  RNum
//
//  Created by Katarina on 23/01/2024.
//

import Foundation

class UserData: ObservableObject {
    let data_url = URL(string: "https://rnumdb-default-rtdb.europe-west1.firebasedatabase.app/users.json")!
    let put_ura = URL(string: "https://rnumdb-default-rtdb.europe-west1.firebasedatabase.app/users/")!
    
    @Published var user: User?
    @Published var users: [User] = []
    
    func fetchUser(username: String) async {
        Task{
            do {
                let (data, _) = try await URLSession.shared.data(from: data_url)
                
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                
                let users = try decoder.decode([String: User].self, from: data)
                user = users.values.first { $0.username == username }
                
                if user == nil {
                    try await createUser(username: username)
                }
            } catch let error {
                print(error)
            }
        }
    }
    
    func createUser(username: String) async throws {
        do {
            let newUser: User = User(username: username, highscore: 0)
            let encorder = JSONEncoder()
            let json = try encorder.encode(newUser)
            
            var request = URLRequest(url: data_url)
            request.httpMethod = "POST"
            request.httpBody = json
            
            let (_, response) = try await URLSession.shared.data(for: request)
            print(response)
        } catch let error {
            print(error)
        }
    }
    
    func updateHighscore(newHighscore: Int) async {
        guard var currentUser = user else {
            return
        }

        currentUser.highscore = newHighscore

        do {
            let encoder = JSONEncoder()
            let jsonData = try encoder.encode(currentUser)

            var request = URLRequest(url: put_ura.appendingPathComponent("\(currentUser.id).json"))
            request.httpMethod = "PATCH"
            request.httpBody = jsonData

            let (_, response) = try await URLSession.shared.data(for: request)
            print(response)
            
            user = currentUser
        } catch let error {
            print(error)
        }
    }
    
    func fetchAllUsersByHighscore() async {
        do {
            let (data, _) = try await URLSession.shared.data(from: data_url)
            
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            
            let fetchedUsers = try decoder.decode([String: User].self, from: data)
            users = fetchedUsers.values.sorted { $0.highscore > $1.highscore }
            
        } catch let error {
            print(error)
        }
    }
}
