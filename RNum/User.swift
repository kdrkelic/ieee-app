import Foundation

struct User: Identifiable, Codable {
    var id = UUID().uuidString
    var username: String
    var highscore: Int
}
