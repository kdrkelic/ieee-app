//
//  ContentView.swift
//  RNum
//
//  Created by Katarina on 23/01/2024.
//

import SwiftUI

struct ContentView: View {
    @State var username = ""
    
    var body: some View {
        NavigationView{
            ZStack{
                Image("logo")
                    .resizable()
                    .edgesIgnoringSafeArea(.all)
                Circle()
                    .scale(1.7)
                    .foregroundColor(.white .opacity(0.15))
                Circle()
                    .scale(1.35)
                    .foregroundColor(.white)
                VStack{
                    Text("NuMbGuess")
                        .font(.largeTitle)
                        .foregroundStyle(.black)
                    TextField("Username", text: $username)
                        .padding()
                        .frame(width: 300, height: 50)
                        .background(Color.black .opacity(0.05))
                        .cornerRadius(10)
                    NavigationLink(destination: MainView(username: $username)){
                        Text("START")
                            .foregroundStyle(.black)
                            .frame(width: 150, height: 50)
                            .background(Color.green .opacity(0.6))
                            .cornerRadius(10)
                        
                        
                    }
                }
                
            }
            
        }
        
    }
}

#Preview {
    ContentView()
}
