//
//  GameView.swift
//  RNum
//
//  Created by Katarina on 23/01/2024.
//

import SwiftUI

struct GameView: View {
    @EnvironmentObject var user: UserData
    @State private var numberToGuess = Int.random(in: 0 ... 999)
    @State private var guess = 0
    
    @State private var feedback = ""
    @State private var GamePlays = 0.0
    
    @State var points = 0
    
    let formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter
    }()
    
    var body: some View {
        VStack{
            Spacer()
            Text("Try to guess the number")
                .font(.largeTitle)
                .padding()
            Text("\(Int(numberToGuess))")
                .font(.title)
                .multilineTextAlignment(.center)
                .padding()
                .frame(width: 148, height: 59,alignment: .center)
            
            Spacer()
            Text("\(feedback)")
            Spacer()
            TextField("", value: $guess, formatter: formatter)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
                .background(Color.green .opacity(0.5))
                .cornerRadius(10)
            
            Spacer()
            Button(action:{
                if guess == numberToGuess{
                    Task {
                        if (user.user != nil){
                            print("USER")
                            let newUser = user.user!
                            print(newUser)
                            if (newUser.highscore < points) {
                                await user.updateHighscore(newHighscore: points)
                            }
                        }
                    }
                    feedback = "YOU GOT IT!"
                    points += 100
                    numberToGuess = Int.random(in: 0 ... 999)
                }
                else if guess < numberToGuess {
                    feedback = "Too low, try higher"
                }
                else{
                    feedback = "Too high, try lower"
                }
                GamePlays += 1
            }, label:{
                Text("Play")
                    .padding()
                    .frame(width: 150, height: 50)
                    .background(Color.black .opacity(0.05))
                    .cornerRadius(10)
            })
            Text ("Score: \(Int(points))")
            Text("\(user.user?.username ?? "Guest") highscore: \(Int(user.user?.highscore ?? 0))")
            HStack{
                ForEach(Array(String(format: "%03d",guess)), id: \.self){digit in Image("\(digit)")}
            }
            Spacer()
            Text("Number of times played: \(Int(GamePlays))")
        }.padding()
    }
}

#Preview {
    GameView()
}
