//
//  UserRow.swift
//  RNum
//
//  Created by Katarina on 23/01/2024.
//

import SwiftUI

struct UserRow: View {
    
    @Binding var user: User
    
    var body: some View {
        HStack{
            Text("\(user.username)")
            Text("\(Int(user.highscore))")
        }
    }
}
