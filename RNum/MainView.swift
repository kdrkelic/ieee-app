//
//  MainView.swift
//  RNum
//
//  Created by Katarina on 23/01/2024.
//

import SwiftUI

struct MainView: View {
    @Binding var username: String
    @StateObject var user = UserData()
    
    var body: some View {
        TabView{
            GameView().tabItem { Label("Game", systemImage: "gamecontroller") }
            Leaderboards().tabItem { Label("Leaderboards", systemImage: "trophy") }
        }.environmentObject(user)
            .task {
            await user.fetchUser(username: username)
                await user.fetchAllUsersByHighscore()
        }
    }
}
