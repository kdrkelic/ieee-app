//
//  Leaderboards.swift
//  RNum
//
//  Created by Katarina on 23/01/2024.
//

import SwiftUI

struct Leaderboards: View {
    
    @EnvironmentObject var userData: UserData
    
    var body: some View {
        VStack{
            List($userData.users){ user in
                UserRow(user: user)
            }.listStyle(.plain)
        }
    }
}
